package SantiagoBrozt.persistencia;

import SantiagoBrozt.modelo.Persona;
import SantiagoBrozt.modelo.ProxyTelefonos;
import SantiagoBrozt.modelo.Telefono;

import java.sql.*;
import java.util.HashSet;
import java.util.Set;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashSet;
import java.util.Set;

public class PersonaDao {
    private String URL = "jdbc:mysql://localhost:3306/tu_basedatos";
    private String USUARIO = "usuario";
    private String CONTRASENIA = "contrasenia";

    public PersonaDao(String url, String usuario, String contrasenia) {
        URL = url;
        USUARIO = usuario;
        CONTRASENIA = contrasenia;
    }

    private Connection obtenerConexion() {
        try {
            return DriverManager.getConnection(URL, USUARIO, CONTRASENIA);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public Persona personaPorId(int id) {
        String sql = "SELECT p.nombre FROM personas p WHERE p.id = ?";
        try (Connection conn = obtenerConexion();
             PreparedStatement statement = conn.prepareStatement(sql)) {
            statement.setInt(1, id);
            ResultSet result = statement.executeQuery();
            String nombrePersona = null;
            if (result.next()) {
                nombrePersona = result.getString(1);
            }
            Set<Telefono> telefonos = new HashSet<>();
            return new Persona(id, nombrePersona, new ProxyTelefonos(id, URL, USUARIO, CONTRASENIA, telefonos));

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}