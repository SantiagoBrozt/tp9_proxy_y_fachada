package SantiagoBrozt.modelo;

import SantiagoBrozt.persistencia.PersonaDao;

//TIP To <b>Run</b> code, press <shortcut actionId="Run"/> or
// click the <icon src="AllIcons.Actions.Execute"/> icon in the gutter.
public class Main {
    public static final String URL = "jdbc:mysql://localhost:3306/tp9_proxy";
    public static final String USUARIO = "SantiagoBrozt";
    public static final String COTRASENIA = "okQFuK8ohJKeeeiK";

    public static void main(String args[]) {
        PersonaDao dao = new PersonaDao(URL, USUARIO, COTRASENIA);
        Persona p = dao.personaPorId(1);
        System.out.println(p.nombre());
        for (Telefono telefono : p.telefonos()) {
            System.out.println(telefono);
        }
    }

}