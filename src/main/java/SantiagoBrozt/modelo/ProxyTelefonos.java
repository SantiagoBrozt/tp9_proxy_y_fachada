package SantiagoBrozt.modelo;

import java.sql.*;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public class ProxyTelefonos implements Set<Telefono> {
    private final int ID_PERSONA;
    private final String URL;
    private final String USUARIO;
    private final String CONTRASENIA;
    private Set <Telefono> telefonos;

    public ProxyTelefonos(int idPersona, String url, String usuario, String contrasenia, Set<Telefono> telefonos) {
        this.ID_PERSONA = idPersona;
        this.URL = url;
        this.USUARIO = usuario;
        this.CONTRASENIA = contrasenia;
        this.telefonos = telefonos;
    }

    private void cargarTelefonos() {
            try (Connection conn = DriverManager.getConnection(URL, USUARIO, CONTRASENIA)) {
                String sql = "SELECT numero FROM telefonos WHERE idPersona = ?";
                PreparedStatement statement = conn.prepareStatement(sql);
                statement.setInt(1, ID_PERSONA);
                ResultSet result = statement.executeQuery();
                while (result.next()) {
                    telefonos.add(new Telefono(result.getString("numero")));
                }
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
        }

    @Override
    public <T> T[] toArray(T[] a) {
        cargarTelefonos();
        return telefonos.toArray(a);
    }

    @Override
    public int size() {
        return telefonos.size();
    }

    @Override
    public boolean isEmpty() {
        return telefonos.isEmpty();
    }

    @Override
    public boolean contains(Object o) {
        return telefonos.contains(o);
    }

    @Override
    public Iterator<Telefono> iterator() {
        return telefonos.iterator();
    }

    @Override
    public Object[] toArray() {
        return telefonos.toArray();
    }

    @Override
    public boolean add(Telefono telefono) {
        return telefonos.add(telefono);
    }

    @Override
    public boolean remove(Object o) {
        return telefonos.remove(o);
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        return telefonos.containsAll(c);
    }

    @Override
    public boolean addAll(Collection<? extends Telefono> c) {
        return telefonos.addAll(c);
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        return telefonos.retainAll(c);
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        return telefonos.removeAll(c);
    }

    @Override
    public void clear() {
        telefonos.clear();
    }
}
